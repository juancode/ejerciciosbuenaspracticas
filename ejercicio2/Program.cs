﻿using System;
using System.Collections.Generic;
using ejercicio2.Entidades;
namespace ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando...");
            
            TaxiAuto auto = new TaxiAuto("Distrito Abancay", 301);
            MostrarRutas(auto);
            ValidarVelocidad(auto);
        }

        private static void ValidarVelocidad(TaxiAuto auto)
        {
            bool stateVelocity = auto.MaximaVelocidad(auto.Velocidad);
            if (stateVelocity)
            {
                Console.WriteLine($"velocidad permitida");
            }
            else
            {
                Console.WriteLine($"La velocidad excedio el limite");
            }
        }

        private static void MostrarRutas(TaxiAuto auto)
        {
            List<string> rutas = auto.GetRutas();
            int cont = 1;
            foreach (var item in rutas)
            {
                Console.WriteLine($"Ruta{cont++}::{item}");
            }
        }
    }
}
