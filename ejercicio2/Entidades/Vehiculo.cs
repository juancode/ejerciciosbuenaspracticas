using System;
namespace ejercicio2.Entidades
{
    public abstract class Vehiculo
    {
        public string Color { get; set; }
        public string Placa { get; set; }
        public int NroRuedas { get; set; }
        public bool MaximaVelocidad(int velocidad)
        {

            if (velocidad > 300)
            {
                return false;

            }
            else
            {
                return true;
            }
        }
    }
}