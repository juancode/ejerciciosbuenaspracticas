
using System.Collections.Generic;
using ejercicio2.Interface;

namespace ejercicio2.Entidades
{
    public class TaxiAuto : Vehiculo, ITaxiAuto
    {
        string juridiccion;
        int velocidad;
        public TaxiAuto(string juridic, int velocidad)
        {
            this.velocidad = velocidad;
            this.juridiccion = juridic;
        }
        public int Velocidad { get {return this.velocidad;} set{ value=this.velocidad;} }

        public List<string> GetRutas()
        {
            List<string> rutas = AddRutas(juridiccion);
            return rutas;
        }

        private static List<string> AddRutas(string name)
        {
            List<string> rutas = new List<string>();
            rutas.Add($"Americas, Publo joven::Sector-{name}");
            rutas.Add($"Publo joven, Unamba::Sector-{name}");
            rutas.Add($"Unamba, Bellavista::Sector-{name}");
            rutas.Add($"Bellavista, Arenas::Sector-{name}");
            return rutas;
        }

       
    }
}